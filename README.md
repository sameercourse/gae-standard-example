 ./mvnw -DskipTests spring-boot:run
 
 #to run the app locally
 
 8. Deploying the Application into App Engine
First, initialize the Project to be able to run App Engine applications. We'll initialize the project to run in the US Central region:

$ gcloud app create --region us-central
You are creating an app for project [...].
WARNING: Creating an App Engine application for a project is irreversible and the region
cannot be changed. More information about regions is at
https://cloud.google.com/appengine/docs/locations
You can alternatively list all available regions `gcloud app regions list` and pick one.

Then, deploy your application into App Engine environment, run mvn appengine:deploy:

$ ./mvnw -DskipTests package appengine:deploy
... first time deploy may take a couple of mintues
First time deployment may take several minutes. Behind the scenes, it's provisioning runtime environment, network, load balancers, and starting your application.

After the application is deployed, you can visit it by opening the URL http://<project-id>.appspot.com in your web browser. Or, type:

$ gcloud app browse
... [It may print out the URL for your app]
Optionally, you can instead purchase and use a top-level domain name for your app, or use one that you have already registered.

In this step, you set up a simple Spring Boot application and ran and deployed your application on App Engine.